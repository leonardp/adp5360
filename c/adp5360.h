#ifndef ZEPHYR_DRIVERS_ADP5360_H_
#define ZEPHYR_DRIVERS_ADP5360_H_

#define ADP5360_MM_ID_ADDR 0x00
union MM_ID {
	int8_t reg;
	struct {
		int8_t MODEL: 4;
		int8_t MANUF: 4;
	} __packed;
};

#define ADP5360_REV_ID_ADDR 0x01
union REV_ID {
	int8_t reg;
	struct {
		int8_t REV: 4;
		int8_t R: 4;
	} __packed;
};

#define ADP5360_CHARGER_VBUS_ILIM_ADDR 0x02
union CHARGER_VBUS_ILIM {
	int8_t reg;
	struct {
		int8_t ILIM: 3;
		int8_t VSYSTEM: 1;
		int8_t R: 1;
		int8_t VADPICHG: 3;
	} __packed;
};

#define ADP5360_CHARGER_TERMINATION_SETTING_ADDR 0x03
union CHARGER_TERMINATION_SETTING {
	int8_t reg;
	struct {
		int8_t ITRK_DEAD: 2;
		int8_t VTRM: 6;
	} __packed;
};

#define ADP5360_CHARGER_CURRENT_SETTING_ADDR 0x04
union CHARGER_CURRENT_SETTING {
	int8_t reg;
	struct {
		int8_t ICHG: 5;
		int8_t IEND: 3;
	} __packed;
};

#define ADP5360_CHARGER_VOLTAGE_THRESHOLD_ADDR 0x05
union CHARGER_VOLTAGE_THRESHOLD {
	int8_t reg;
	struct {
		int8_t VWEAK: 3;
		int8_t VTRK_DEAD: 2;
		int8_t VRCH: 2;
		int8_t DIS_RCH: 1;
	} __packed;
};

#define ADP5360_CHARGER_TIMER_SETTING_ADDR 0x06
union CHARGER_TIMER_SETTING {
	int8_t reg;
	struct {
		int8_t CHG_TMR_PERIOD: 2;
		int8_t EN_CHG_TIMER: 1;
		int8_t EN_TEND: 1;
		int8_t R: 4;
	} __packed;
};

#define ADP5360_CHARGER_FUNCTION_SETTING_ADDR 0x07
union CHARGER_FUNCTION_SETTING {
	int8_t reg;
	struct {
		int8_t EN_CHG: 1;
		int8_t EN_ADPICHG: 1;
		int8_t EN_EOC: 1;
		int8_t EN_LDO: 1;
		int8_t OFF_ISOFET: 1;
		int8_t R: 1;
		int8_t ILIM_JEITA_COOL: 1;
		int8_t EN_JEITA: 1;
	} __packed;
};

#define ADP5360_CHARGER_STATUS1_ADDR 0x08
union CHARGER_STATUS1 {
	int8_t reg;
	struct {
		int8_t CHARGER_STATUS: 3;
		int8_t R: 2;
		int8_t VBUS_ILIM: 1;
		int8_t ADPICHG: 1;
		int8_t VBUS_OV: 1;
	} __packed;
};

#define ADP5360_CHARGER_STATUS2_ADDR 0x09
union CHARGER_STATUS2 {
	int8_t reg;
	struct {
		int8_t BAT_CHG_STATUS: 3;
		int8_t BAT_UV_STATUS: 1;
		int8_t BAT_OV_STATUS: 1;
		int8_t THR_STATUS: 3;
	} __packed;
};

#define ADP5360_BATTERY_THERMISTOR_CONTROL_ADDR 0x0A
union BATTERY_THERMISTOR_CONTROL {
	int8_t reg;
	struct {
		int8_t EN_THR: 1;
		int8_t R: 5;
		int8_t ITHR: 2;
	} __packed;
};

#define ADP5360_THERMISTOR_60C_THRESHOLD_ADDR 0x0B
union THERMISTOR_60C_THRESHOLD {
	int8_t reg;
	struct {
		int8_t TEMP_HIGH_60: 8;
	} __packed;
};

#define ADP5360_THERMISTOR_45C_THRESHOLD_ADDR 0x0C
union THERMISTOR_45C_THRESHOLD {
	int8_t reg;
	struct {
		int8_t TEMP_HIGH_45: 8;
	} __packed;
};

#define ADP5360_THERMISTOR_10C_THRESHOLD_ADDR 0x0D
union THERMISTOR_10C_THRESHOLD {
	int8_t reg;
	struct {
		int8_t TEMP_LOW_10: 8;
	} __packed;
};

#define ADP5360_THERMISTOR_0C_THRESHOLD_ADDR 0x0E
union THERMISTOR_0C_THRESHOLD {
	int8_t reg;
	struct {
		int8_t TEMP_LOW_0: 8;
	} __packed;
};

#define ADP5360_THR_VOLTAGE_LOW_ADDR 0x0F
union THR_VOLTAGE_LOW {
	int8_t reg;
	struct {
		int8_t THR_V_LOW: 8;
	} __packed;
};

#define ADP5360_THR_VOLTAGE_HIGH_ADDR 0x10
union THR_VOLTAGE_HIGH {
	int8_t reg;
	struct {
		int8_t THR_V_HIGH: 3;
		int8_t R: 3;
	} __packed;
};

#define ADP5360_BAT_PROT_CONTROL_ADDR 0x11
union BAT_PROT_CONTROL {
	int8_t reg;
	struct {
		int8_t EN_BATPRO: 1;
		int8_t EN_CHGLB: 1;
		int8_t OC_CHG_HICCUP: 1;
		int8_t OC_DIS_HICCUP: 1;
		int8_t ISOFET_OVCHG: 1;
		int8_t R: 3;
	} __packed;
};

#define ADP5360_BAT_PROT_UV_ADDR 0x12
union BAT_PROT_UV {
	int8_t reg;
	struct {
		int8_t DGT_UV_DISCH: 2;
		int8_t HYS_UV_DISCH: 2;
		int8_t UV_DISCH: 4;
	} __packed;
};

#define ADP5360_BAT_PROT_OC_DISCH_ADDR 0x13
union BAT_PROT_OC_DISCH {
	int8_t reg;
	struct {
		int8_t R: 1;
		int8_t DGT_OC_DISCH: 3;
		int8_t RR: 1;
		int8_t OC_DISCH: 3;
	} __packed;
};

#define ADP5360_BAT_PROT_OV_ADDR 0x14
union BAT_PROT_OV {
	int8_t reg;
	struct {
		int8_t DGT_OV_CHG: 1;
		int8_t HYS_OV_CHG: 2;
		int8_t OV_CHG: 5;
	} __packed;
};

#define ADP5360_BAT_PROT_OC_CHG_ADDR 0x15
union BAT_PROT_OC_CHG {
	int8_t reg;
	struct {
		int8_t R: 3;
		int8_t DGT_OC_CHG: 2;
		int8_t OC_CHG: 3;
	} __packed;
};

#define ADP5360_V_SOC_0_ADDR 0x16
union V_SOC_0 {
	int8_t reg;
	struct {
		int8_t V_SOC_0: 8;
	} __packed;
};

#define ADP5360_V_SOC_5_ADDR 0x17
union V_SOC_5 {
	int8_t reg;
	struct {
		int8_t V_SOC_5: 8;
	} __packed;
};

#define ADP5360_V_SOC_11_ADDR 0x18
union V_SOC_11 {
	int8_t reg;
	struct {
		int8_t V_SOC_11: 8;
	} __packed;
};

#define ADP5360_V_SOC_19_ADDR 0x19
union V_SOC_19 {
	int8_t reg;
	struct {
		int8_t V_SOC_19: 8;
	} __packed;
};

#define ADP5360_V_SOC_28_ADDR 0x1A
union V_SOC_28 {
	int8_t reg;
	struct {
		int8_t V_SOC_28: 8;
	} __packed;
};

#define ADP5360_V_SOC_41_ADDR 0x1B
union V_SOC_41 {
	int8_t reg;
	struct {
		int8_t V_SOC_41: 8;
	} __packed;
};

#define ADP5360_V_SOC_55_ADDR 0x1C
union V_SOC_55 {
	int8_t reg;
	struct {
		int8_t V_SOC_55: 8;
	} __packed;
};

#define ADP5360_V_SOC_69_ADDR 0x1D
union V_SOC_69 {
	int8_t reg;
	struct {
		int8_t V_SOC_69: 8;
	} __packed;
};

#define ADP5360_V_SOC_84_ADDR 0x1E
union V_SOC_84 {
	int8_t reg;
	struct {
		int8_t V_SOC_84: 8;
	} __packed;
};

#define ADP5360_V_SOC_100_ADDR 0x1F
union V_SOC_100 {
	int8_t reg;
	struct {
		int8_t V_SOC_100: 8;
	} __packed;
};

#define ADP5360_BAT_CAP_ADDR 0x20
union BAT_CAP {
	int8_t reg;
	struct {
		int8_t BAT_CAP: 8;
	} __packed;
};

#define ADP5360_BAT_SOC_ADDR 0x21
union BAT_SOC {
	int8_t reg;
	struct {
		int8_t BAT_SOC: 7;
		int8_t R: 1;
	} __packed;
};

#define ADP5360_BAT_SOCACM_CTL_ADDR 0x22
union BAT_SOCACM_CTL {
	int8_t reg;
	struct {
		int8_t EN_BATCAP_AGE: 1;
		int8_t EN_BATCAP_TEMP: 1;
		int8_t R: 2;
		int8_t BATCAP_TEMP: 2;
		int8_t BATCAP_AGE: 2;
	} __packed;
};

#define ADP5360_BAT_SOCACM_H_ADDR 0x23
union BAT_SOCACM_H {
	int8_t reg;
	struct {
		int8_t BAT_SOCACM: 8;
	} __packed;
};

#define ADP5360_BAT_SOCACM_L_ADDR 0x24
union BAT_SOCACM_L {
	int8_t reg;
	struct {
		int8_t R: 4;
		int8_t BAT_SOCACM: 4;
	} __packed;
};

#define ADP5360_VBAT_READ_H_ADDR 0x25
union VBAT_READ_H {
	int8_t reg;
	struct {
		int8_t VBAT_READ: 8;
	} __packed;
};

#define ADP5360_VBAT_READ_L_ADDR 0x26
union VBAT_READ_L {
	int8_t reg;
	struct {
		int8_t R: 3;
		int8_t VBAT_READ: 5;
	} __packed;
};

#define ADP5360_FUEL_GAUGE_MODE_ADDR 0x27
union FUEL_GAUGE_MODE {
	int8_t reg;
	struct {
		int8_t EN_FG: 1;
		int8_t FG_MODE: 1;
		int8_t SLP_TIME: 2;
		int8_t SLP_CURR: 2;
		int8_t SOC_LOW_TH: 2;
	} __packed;
};

#define ADP5360_SOC_RESET_ADDR 0x28
union SOC_RESET {
	int8_t reg;
	struct {
		int8_t R: 7;
		int8_t SOC_RESET: 1;
	} __packed;
};

#define ADP5360_BUCK_CONF_ADDR 0x29
union BUCK_CONF {
	int8_t reg;
	struct {
		int8_t EN_BUCK: 1;
		int8_t DISCH_BUCK: 1;
		int8_t STP_BUCK: 1;
		int8_t BUCK_MODE: 1;
		int8_t BUCK_ILIM: 2;
		int8_t BUCK_SS: 2;
	} __packed;
};

#define ADP5360_BUCK_VOLT_SET_ADDR 0x2A
union BUCK_VOLT_SET {
	int8_t reg;
	struct {
		int8_t VOUT_BUCK: 6;
		int8_t BUCK_DLY: 2;
	} __packed;
};

#define ADP5360_BUCKBST_CONF_ADDR 0x2B
union BUCKBST_CONF {
	int8_t reg;
	struct {
		int8_t EN_BUCKBST: 1;
		int8_t DISCH_BUCKBST: 1;
		int8_t STP_BUCKBST: 1;
		int8_t BUCKBST_ILIM: 3;
		int8_t BUCKBST_SS: 2;
	} __packed;
};

#define ADP5360_BUCKBST_VOLT_SET_ADDR 0x2C
union BUCKBST_VOLT_SET {
	int8_t reg;
	struct {
		int8_t VOUT_BUCKBST: 6;
		int8_t BUCKBST_DLY: 2;
	} __packed;
};

#define ADP5360_SUPERVISOR_SET_ADDR 0x2D
union SUPERVISOR_SET {
	int8_t reg;
	struct {
		int8_t RESET_WD: 1;
		int8_t EN_MR_SD: 1;
		int8_t EN_WD: 1;
		int8_t WD_TIME: 2;
		int8_t RESET_TIME: 1;
		int8_t VOUT2_RST: 1;
		int8_t VOUT1_RST: 1;
	} __packed;
};

#define ADP5360_FAULT_ADDR 0x2E
union FAULT {
	int8_t reg;
	struct {
		int8_t TSD_110: 1;
		int8_t R: 1;
		int8_t WD_TIMEOUT: 1;
		int8_t RR: 1;
		int8_t BAT_CHGOV: 1;
		int8_t BAT_CHGOC: 1;
		int8_t BAT_OC: 1;
		int8_t BAT_UV: 1;
	} __packed;
};

#define ADP5360_PGOOD_STATUS_ADDR 0x2F
union PGOOD_STATUS {
	int8_t reg;
	struct {
		int8_t VOUT1OK: 1;
		int8_t VOUT2OK: 1;
		int8_t BAT_OK: 1;
		int8_t VBUSOK: 1;
		int8_t CHG_CMPLT: 1;
		int8_t MR_PRESS: 1;
		int8_t R: 2;
	} __packed;
};

#define ADP5360_PGOOD1_MASK_ADDR 0x30
union PGOOD1_MASK {
	int8_t reg;
	struct {
		int8_t VOUT1OK_MASK1: 1;
		int8_t VOUT2OK_MASK1: 1;
		int8_t BATOK_MASK1: 1;
		int8_t VBUSOK_MASK1: 1;
		int8_t CHGCMPLT_MASK1: 1;
		int8_t R: 2;
		int8_t PG1_REV: 1;
	} __packed;
};

#define ADP5360_PGOOD2_MASK_ADDR 0x31
union PGOOD2_MASK {
	int8_t reg;
	struct {
		int8_t VOUT1OK_MASK2: 1;
		int8_t VOUT2OK_MASK2: 1;
		int8_t BATOK_MASK2: 1;
		int8_t VBUSOK_MASK2: 1;
		int8_t CHGCMPLT_MASK2: 1;
		int8_t R: 2;
		int8_t PG2_REV: 1;
	} __packed;
};

#define ADP5360_INT_ENABLE1_ADDR 0x32
union INT_ENABLE1 {
	int8_t reg;
	struct {
		int8_t EN_VBUS_INT: 1;
		int8_t EN_CHG_INT: 1;
		int8_t EN_BAT_INT: 1;
		int8_t EN_THR_INT: 1;
		int8_t EN_BATPRO_INT: 1;
		int8_t EN_ADPICHG_INT: 1;
		int8_t EN_SOCACM_INT: 1;
		int8_t EN_SOCLOW_INT: 1;
	} __packed;
};

#define ADP5360_INT_ENABLE2_ADDR 0x33
union INT_ENABLE2 {
	int8_t reg;
	struct {
		int8_t R: 4;
		int8_t EN_BUCKBSTPG_INT: 1;
		int8_t EN_BUCKPG_INT: 1;
		int8_t EN_WD_INT: 1;
		int8_t EN_MR_INT: 1;
	} __packed;
};

#define ADP5360_INT_FLAG1_ADDR 0x34
union INT_FLAG1 {
	int8_t reg;
	struct {
		int8_t VBUS_INT: 1;
		int8_t CHG_INT: 1;
		int8_t BAT_INT: 1;
		int8_t THR_INT: 1;
		int8_t BATPRO_INT: 1;
		int8_t ADPICHG_INT: 1;
		int8_t SOCACM_INT: 1;
		int8_t SOCLOW_INT: 1;
	} __packed;
};

#define ADP5360_INT_FLAG2_ADDR 0x35
union INT_FLAG2 {
	int8_t reg;
	struct {
		int8_t R: 4;
		int8_t BUCKBSTPG_INT: 1;
		int8_t BUCKPG_INT: 1;
		int8_t WD_INT: 1;
		int8_t MR_INT: 1;
	} __packed;
};

#define ADP5360_SHIPMODE_ADDR 0x36
union SHIPMODE {
	int8_t reg;
	struct {
		int8_t EN_SHIPMODE: 1;
		int8_t R: 7;
	} __packed;
};

#endif ZEPHYR_DRIVERS_ADP5360_H_