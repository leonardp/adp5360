import json
from pprint import pprint

c_fname = "./c/adp5360.h"
mpy_fname = "./mpy/GEN_adp5360.py"

with open("registers.json", "r") as f:
    regmap = json.load(f)

#
# generate zephyr header
#

with open(c_fname, "w") as f:
    f.write("#ifndef ZEPHYR_DRIVERS_ADP5360_H_\n")
    f.write("#define ZEPHYR_DRIVERS_ADP5360_H_\n")
    f.write("\n")
    for reg in regmap["registers"]:
        f.write(f"#define ADP5360_{reg['name']}_ADDR {reg['addr']}\n")
        f.write(f"union {reg['name']} {{\n")
        f.write("\tint8_t reg;\n")
        f.write("\tstruct {\n")
        for d in reg["fields"]:
            for name, length in d.items():
                f.write(f"\t\tint8_t {name}: {length};\n")
        f.write("\t} __packed;\n")
        f.write("};\n")
        f.write("\n")
    f.write("#endif ZEPHYR_DRIVERS_ADP5360_H_")

#
# generate micropython class
#

indent = "    "
with open(mpy_fname, "w") as f:
    f.write('from machine import Pin\n\n')
    f.write('class ADP5360:\n')
    for reg in regmap["registers"]:
        f.write(f'{indent}{reg["name"]} = {{\n')
        f.write(f'{2*indent}"addr": {reg["addr"]},\n')
        f.write(f'{2*indent}"fields": {{\n')
        os = 0
        for d in reg["fields"]:
            for name, length in d.items():
                f.write(f'{3*indent}"{name}": {{\n')
                f.write(f'{4*indent}"len": {length},\n')
                f.write(f'{4*indent}"os": {os},\n')
                f.write(f'{3*indent}}},\n')
                os += length
        f.write(f'{2*indent}}},\n')
        f.write(f'{indent}}}\n')
