import os
import gc
import machine

def gc_print():
    print("MEM Free:")
    gc.collect()
    print("free: {} alloc: {}".format(gc.mem_free(), gc.mem_alloc()))

def storage_free():
    print("Storage:")
    s = os.statvfs("/")
    tot = s[0]*s[2]
    free = s[0]*s[3]
    used = tot - free
    print("tot: {} used: {} free: {}".format(tot, used, free))

print("ls:", os.listdir())
storage_free()

i2c = machine.I2C(scl=machine.Pin(4), sda=machine.Pin(9), freq=100000)
print("I2C Scan:", i2c.scan())

gc_print()

from adp5360 import ADP5360
a = ADP5360(i2c)

gc_print()
