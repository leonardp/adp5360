from machine import Pin

class ADP5360:
    MM_ID = {
        "addr": 0x00,
        "fields": {
            "MODEL": {
                "len": 4,
                "os": 0,
            },
            "MANUF": {
                "len": 4,
                "os": 4,
            },
        },
    }
    REV_ID = {
        "addr": 0x01,
        "fields": {
            "REV": {
                "len": 4,
                "os": 0,
            },
            "R": {
                "len": 4,
                "os": 4,
            },
        },
    }
    CHARGER_VBUS_ILIM = {
        "addr": 0x02,
        "fields": {
            "ILIM": {
                "len": 3,
                "os": 0,
            },
            "VSYSTEM": {
                "len": 1,
                "os": 3,
            },
            "R": {
                "len": 1,
                "os": 4,
            },
            "VADPICHG": {
                "len": 3,
                "os": 5,
            },
        },
    }
    CHARGER_TERMINATION_SETTING = {
        "addr": 0x03,
        "fields": {
            "ITRK_DEAD": {
                "len": 2,
                "os": 0,
            },
            "VTRM": {
                "len": 6,
                "os": 2,
            },
        },
    }
    CHARGER_CURRENT_SETTING = {
        "addr": 0x04,
        "fields": {
            "ICHG": {
                "len": 5,
                "os": 0,
            },
            "IEND": {
                "len": 3,
                "os": 5,
            },
        },
    }
    CHARGER_VOLTAGE_THRESHOLD = {
        "addr": 0x05,
        "fields": {
            "VWEAK": {
                "len": 3,
                "os": 0,
            },
            "VTRK_DEAD": {
                "len": 2,
                "os": 3,
            },
            "VRCH": {
                "len": 2,
                "os": 5,
            },
            "DIS_RCH": {
                "len": 1,
                "os": 7,
            },
        },
    }
    CHARGER_TIMER_SETTING = {
        "addr": 0x06,
        "fields": {
            "CHG_TMR_PERIOD": {
                "len": 2,
                "os": 0,
            },
            "EN_CHG_TIMER": {
                "len": 1,
                "os": 2,
            },
            "EN_TEND": {
                "len": 1,
                "os": 3,
            },
            "R": {
                "len": 4,
                "os": 4,
            },
        },
    }
    CHARGER_FUNCTION_SETTING = {
        "addr": 0x07,
        "fields": {
            "EN_CHG": {
                "len": 1,
                "os": 0,
            },
            "EN_ADPICHG": {
                "len": 1,
                "os": 1,
            },
            "EN_EOC": {
                "len": 1,
                "os": 2,
            },
            "EN_LDO": {
                "len": 1,
                "os": 3,
            },
            "OFF_ISOFET": {
                "len": 1,
                "os": 4,
            },
            "R": {
                "len": 1,
                "os": 5,
            },
            "ILIM_JEITA_COOL": {
                "len": 1,
                "os": 6,
            },
            "EN_JEITA": {
                "len": 1,
                "os": 7,
            },
        },
    }
    CHARGER_STATUS1 = {
        "addr": 0x08,
        "fields": {
            "CHARGER_STATUS": {
                "len": 3,
                "os": 0,
            },
            "R": {
                "len": 2,
                "os": 3,
            },
            "VBUS_ILIM": {
                "len": 1,
                "os": 5,
            },
            "ADPICHG": {
                "len": 1,
                "os": 6,
            },
            "VBUS_OV": {
                "len": 1,
                "os": 7,
            },
        },
    }
    CHARGER_STATUS2 = {
        "addr": 0x09,
        "fields": {
            "BAT_CHG_STATUS": {
                "len": 3,
                "os": 0,
            },
            "BAT_UV_STATUS": {
                "len": 1,
                "os": 3,
            },
            "BAT_OV_STATUS": {
                "len": 1,
                "os": 4,
            },
            "THR_STATUS": {
                "len": 3,
                "os": 5,
            },
        },
    }
    BATTERY_THERMISTOR_CONTROL = {
        "addr": 0x0A,
        "fields": {
            "EN_THR": {
                "len": 1,
                "os": 0,
            },
            "R": {
                "len": 5,
                "os": 1,
            },
            "ITHR": {
                "len": 2,
                "os": 6,
            },
        },
    }
    THERMISTOR_60C_THRESHOLD = {
        "addr": 0x0B,
        "fields": {
            "TEMP_HIGH_60": {
                "len": 8,
                "os": 0,
            },
        },
    }
    THERMISTOR_45C_THRESHOLD = {
        "addr": 0x0C,
        "fields": {
            "TEMP_HIGH_45": {
                "len": 8,
                "os": 0,
            },
        },
    }
    THERMISTOR_10C_THRESHOLD = {
        "addr": 0x0D,
        "fields": {
            "TEMP_LOW_10": {
                "len": 8,
                "os": 0,
            },
        },
    }
    THERMISTOR_0C_THRESHOLD = {
        "addr": 0x0E,
        "fields": {
            "TEMP_LOW_0": {
                "len": 8,
                "os": 0,
            },
        },
    }
    THR_VOLTAGE_LOW = {
        "addr": 0x0F,
        "fields": {
            "THR_V_LOW": {
                "len": 8,
                "os": 0,
            },
        },
    }
    THR_VOLTAGE_HIGH = {
        "addr": 0x10,
        "fields": {
            "THR_V_HIGH": {
                "len": 3,
                "os": 0,
            },
            "R": {
                "len": 3,
                "os": 3,
            },
        },
    }
    BAT_PROT_CONTROL = {
        "addr": 0x11,
        "fields": {
            "EN_BATPRO": {
                "len": 1,
                "os": 0,
            },
            "EN_CHGLB": {
                "len": 1,
                "os": 1,
            },
            "OC_CHG_HICCUP": {
                "len": 1,
                "os": 2,
            },
            "OC_DIS_HICCUP": {
                "len": 1,
                "os": 3,
            },
            "ISOFET_OVCHG": {
                "len": 1,
                "os": 4,
            },
            "R": {
                "len": 3,
                "os": 5,
            },
        },
    }
    BAT_PROT_UV = {
        "addr": 0x12,
        "fields": {
            "DGT_UV_DISCH": {
                "len": 2,
                "os": 0,
            },
            "HYS_UV_DISCH": {
                "len": 2,
                "os": 2,
            },
            "UV_DISCH": {
                "len": 4,
                "os": 4,
            },
        },
    }
    BAT_PROT_OC_DISCH = {
        "addr": 0x13,
        "fields": {
            "R": {
                "len": 1,
                "os": 0,
            },
            "DGT_OC_DISCH": {
                "len": 3,
                "os": 1,
            },
            "RR": {
                "len": 1,
                "os": 4,
            },
            "OC_DISCH": {
                "len": 3,
                "os": 5,
            },
        },
    }
    BAT_PROT_OV = {
        "addr": 0x14,
        "fields": {
            "DGT_OV_CHG": {
                "len": 1,
                "os": 0,
            },
            "HYS_OV_CHG": {
                "len": 2,
                "os": 1,
            },
            "OV_CHG": {
                "len": 5,
                "os": 3,
            },
        },
    }
    BAT_PROT_OC_CHG = {
        "addr": 0x15,
        "fields": {
            "R": {
                "len": 3,
                "os": 0,
            },
            "DGT_OC_CHG": {
                "len": 2,
                "os": 3,
            },
            "OC_CHG": {
                "len": 3,
                "os": 5,
            },
        },
    }
    V_SOC_0 = {
        "addr": 0x16,
        "fields": {
            "V_SOC_0": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_5 = {
        "addr": 0x17,
        "fields": {
            "V_SOC_5": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_11 = {
        "addr": 0x18,
        "fields": {
            "V_SOC_11": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_19 = {
        "addr": 0x19,
        "fields": {
            "V_SOC_19": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_28 = {
        "addr": 0x1A,
        "fields": {
            "V_SOC_28": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_41 = {
        "addr": 0x1B,
        "fields": {
            "V_SOC_41": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_55 = {
        "addr": 0x1C,
        "fields": {
            "V_SOC_55": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_69 = {
        "addr": 0x1D,
        "fields": {
            "V_SOC_69": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_84 = {
        "addr": 0x1E,
        "fields": {
            "V_SOC_84": {
                "len": 8,
                "os": 0,
            },
        },
    }
    V_SOC_100 = {
        "addr": 0x1F,
        "fields": {
            "V_SOC_100": {
                "len": 8,
                "os": 0,
            },
        },
    }
    BAT_CAP = {
        "addr": 0x20,
        "fields": {
            "BAT_CAP": {
                "len": 8,
                "os": 0,
            },
        },
    }
    BAT_SOC = {
        "addr": 0x21,
        "fields": {
            "BAT_SOC": {
                "len": 7,
                "os": 0,
            },
            "R": {
                "len": 1,
                "os": 7,
            },
        },
    }
    BAT_SOCACM_CTL = {
        "addr": 0x22,
        "fields": {
            "EN_BATCAP_AGE": {
                "len": 1,
                "os": 0,
            },
            "EN_BATCAP_TEMP": {
                "len": 1,
                "os": 1,
            },
            "R": {
                "len": 2,
                "os": 2,
            },
            "BATCAP_TEMP": {
                "len": 2,
                "os": 4,
            },
            "BATCAP_AGE": {
                "len": 2,
                "os": 6,
            },
        },
    }
    BAT_SOCACM_H = {
        "addr": 0x23,
        "fields": {
            "BAT_SOCACM": {
                "len": 8,
                "os": 0,
            },
        },
    }
    BAT_SOCACM_L = {
        "addr": 0x24,
        "fields": {
            "R": {
                "len": 4,
                "os": 0,
            },
            "BAT_SOCACM": {
                "len": 4,
                "os": 4,
            },
        },
    }
    VBAT_READ_H = {
        "addr": 0x25,
        "fields": {
            "VBAT_READ": {
                "len": 8,
                "os": 0,
            },
        },
    }
    VBAT_READ_L = {
        "addr": 0x26,
        "fields": {
            "R": {
                "len": 3,
                "os": 0,
            },
            "VBAT_READ": {
                "len": 5,
                "os": 3,
            },
        },
    }
    FUEL_GAUGE_MODE = {
        "addr": 0x27,
        "fields": {
            "EN_FG": {
                "len": 1,
                "os": 0,
            },
            "FG_MODE": {
                "len": 1,
                "os": 1,
            },
            "SLP_TIME": {
                "len": 2,
                "os": 2,
            },
            "SLP_CURR": {
                "len": 2,
                "os": 4,
            },
            "SOC_LOW_TH": {
                "len": 2,
                "os": 6,
            },
        },
    }
    SOC_RESET = {
        "addr": 0x28,
        "fields": {
            "R": {
                "len": 7,
                "os": 0,
            },
            "SOC_RESET": {
                "len": 1,
                "os": 7,
            },
        },
    }
    BUCK_CONF = {
        "addr": 0x29,
        "fields": {
            "EN_BUCK": {
                "len": 1,
                "os": 0,
            },
            "DISCH_BUCK": {
                "len": 1,
                "os": 1,
            },
            "STP_BUCK": {
                "len": 1,
                "os": 2,
            },
            "BUCK_MODE": {
                "len": 1,
                "os": 3,
            },
            "BUCK_ILIM": {
                "len": 2,
                "os": 4,
            },
            "BUCK_SS": {
                "len": 2,
                "os": 6,
            },
        },
    }
    BUCK_VOLT_SET = {
        "addr": 0x2A,
        "fields": {
            "VOUT_BUCK": {
                "len": 6,
                "os": 0,
            },
            "BUCK_DLY": {
                "len": 2,
                "os": 6,
            },
        },
    }
    BUCKBST_CONF = {
        "addr": 0x2B,
        "fields": {
            "EN_BUCKBST": {
                "len": 1,
                "os": 0,
            },
            "DISCH_BUCKBST": {
                "len": 1,
                "os": 1,
            },
            "STP_BUCKBST": {
                "len": 1,
                "os": 2,
            },
            "BUCKBST_ILIM": {
                "len": 3,
                "os": 3,
            },
            "BUCKBST_SS": {
                "len": 2,
                "os": 6,
            },
        },
    }
    BUCKBST_VOLT_SET = {
        "addr": 0x2C,
        "fields": {
            "VOUT_BUCKBST": {
                "len": 6,
                "os": 0,
            },
            "BUCKBST_DLY": {
                "len": 2,
                "os": 6,
            },
        },
    }
    SUPERVISOR_SET = {
        "addr": 0x2D,
        "fields": {
            "RESET_WD": {
                "len": 1,
                "os": 0,
            },
            "EN_MR_SD": {
                "len": 1,
                "os": 1,
            },
            "EN_WD": {
                "len": 1,
                "os": 2,
            },
            "WD_TIME": {
                "len": 2,
                "os": 3,
            },
            "RESET_TIME": {
                "len": 1,
                "os": 5,
            },
            "VOUT2_RST": {
                "len": 1,
                "os": 6,
            },
            "VOUT1_RST": {
                "len": 1,
                "os": 7,
            },
        },
    }
    FAULT = {
        "addr": 0x2E,
        "fields": {
            "TSD_110": {
                "len": 1,
                "os": 0,
            },
            "R": {
                "len": 1,
                "os": 1,
            },
            "WD_TIMEOUT": {
                "len": 1,
                "os": 2,
            },
            "RR": {
                "len": 1,
                "os": 3,
            },
            "BAT_CHGOV": {
                "len": 1,
                "os": 4,
            },
            "BAT_CHGOC": {
                "len": 1,
                "os": 5,
            },
            "BAT_OC": {
                "len": 1,
                "os": 6,
            },
            "BAT_UV": {
                "len": 1,
                "os": 7,
            },
        },
    }
    PGOOD_STATUS = {
        "addr": 0x2F,
        "fields": {
            "VOUT1OK": {
                "len": 1,
                "os": 0,
            },
            "VOUT2OK": {
                "len": 1,
                "os": 1,
            },
            "BAT_OK": {
                "len": 1,
                "os": 2,
            },
            "VBUSOK": {
                "len": 1,
                "os": 3,
            },
            "CHG_CMPLT": {
                "len": 1,
                "os": 4,
            },
            "MR_PRESS": {
                "len": 1,
                "os": 5,
            },
            "R": {
                "len": 2,
                "os": 6,
            },
        },
    }
    PGOOD1_MASK = {
        "addr": 0x30,
        "fields": {
            "VOUT1OK_MASK1": {
                "len": 1,
                "os": 0,
            },
            "VOUT2OK_MASK1": {
                "len": 1,
                "os": 1,
            },
            "BATOK_MASK1": {
                "len": 1,
                "os": 2,
            },
            "VBUSOK_MASK1": {
                "len": 1,
                "os": 3,
            },
            "CHGCMPLT_MASK1": {
                "len": 1,
                "os": 4,
            },
            "R": {
                "len": 2,
                "os": 5,
            },
            "PG1_REV": {
                "len": 1,
                "os": 7,
            },
        },
    }
    PGOOD2_MASK = {
        "addr": 0x31,
        "fields": {
            "VOUT1OK_MASK2": {
                "len": 1,
                "os": 0,
            },
            "VOUT2OK_MASK2": {
                "len": 1,
                "os": 1,
            },
            "BATOK_MASK2": {
                "len": 1,
                "os": 2,
            },
            "VBUSOK_MASK2": {
                "len": 1,
                "os": 3,
            },
            "CHGCMPLT_MASK2": {
                "len": 1,
                "os": 4,
            },
            "R": {
                "len": 2,
                "os": 5,
            },
            "PG2_REV": {
                "len": 1,
                "os": 7,
            },
        },
    }
    INT_ENABLE1 = {
        "addr": 0x32,
        "fields": {
            "EN_VBUS_INT": {
                "len": 1,
                "os": 0,
            },
            "EN_CHG_INT": {
                "len": 1,
                "os": 1,
            },
            "EN_BAT_INT": {
                "len": 1,
                "os": 2,
            },
            "EN_THR_INT": {
                "len": 1,
                "os": 3,
            },
            "EN_BATPRO_INT": {
                "len": 1,
                "os": 4,
            },
            "EN_ADPICHG_INT": {
                "len": 1,
                "os": 5,
            },
            "EN_SOCACM_INT": {
                "len": 1,
                "os": 6,
            },
            "EN_SOCLOW_INT": {
                "len": 1,
                "os": 7,
            },
        },
    }
    INT_ENABLE2 = {
        "addr": 0x33,
        "fields": {
            "R": {
                "len": 4,
                "os": 0,
            },
            "EN_BUCKBSTPG_INT": {
                "len": 1,
                "os": 4,
            },
            "EN_BUCKPG_INT": {
                "len": 1,
                "os": 5,
            },
            "EN_WD_INT": {
                "len": 1,
                "os": 6,
            },
            "EN_MR_INT": {
                "len": 1,
                "os": 7,
            },
        },
    }
    INT_FLAG1 = {
        "addr": 0x34,
        "fields": {
            "VBUS_INT": {
                "len": 1,
                "os": 0,
            },
            "CHG_INT": {
                "len": 1,
                "os": 1,
            },
            "BAT_INT": {
                "len": 1,
                "os": 2,
            },
            "THR_INT": {
                "len": 1,
                "os": 3,
            },
            "BATPRO_INT": {
                "len": 1,
                "os": 4,
            },
            "ADPICHG_INT": {
                "len": 1,
                "os": 5,
            },
            "SOCACM_INT": {
                "len": 1,
                "os": 6,
            },
            "SOCLOW_INT": {
                "len": 1,
                "os": 7,
            },
        },
    }
    INT_FLAG2 = {
        "addr": 0x35,
        "fields": {
            "R": {
                "len": 4,
                "os": 0,
            },
            "BUCKBSTPG_INT": {
                "len": 1,
                "os": 4,
            },
            "BUCKPG_INT": {
                "len": 1,
                "os": 5,
            },
            "WD_INT": {
                "len": 1,
                "os": 6,
            },
            "MR_INT": {
                "len": 1,
                "os": 7,
            },
        },
    }
    SHIPMODE = {
        "addr": 0x36,
        "fields": {
            "EN_SHIPMODE": {
                "len": 1,
                "os": 0,
            },
            "R": {
                "len": 7,
                "os": 1,
            },
        },
    }

    def __init__(self, i2c, address=0x46, pin_no_int=None):
        self.i2c = i2c
        self.address = address
        if pin_no_int:
            self.pin_int = Pin(pin_no_int, Pin.IN)
        else:
            self.pin_int = None
        self.set_initial()

    def write_reg(self, reg, val):
        return self.i2c.writeto_mem(self.address, reg['addr'], chr(val))

    def read_reg(self, reg):
        raw = self.i2c.readfrom_mem(self.address, reg['addr'], 1)
        return int.from_bytes(raw, 'big')

    def gen_mask(self, length):
        mask = 0
        for b in range(0, length):
            mask |= 1 << b
        return mask

    def get_field(self, reg, field_name):
        reg_val = self.read_reg(reg)
        try:
            f = reg['fields'][field_name]
        except KeyError:
            raise Exception("Unknown field name.")

        mask = self.gen_mask(f['len'])
        #print('regval: {:08b}'.format(reg_val))
        #print('mask: {:08b}'.format(mask))
        #print('bits: {:08b}'.format(mask & reg_val >> f['os']))
        return mask & reg_val >> f['os']

    def set_field(self, reg, field_name, val):
        reg_val = self.read_reg(reg)
        try:
            f = reg['fields'][field_name]
        except KeyError:
            raise Exception("Unknown field name.")
        mask = self.gen_mask(f['len'])
        regnew = (~(mask << f['os']) & 0xff & reg_val) | val << f['os']
        #print('regval: {:08b}'.format(reg_val))
        #print('mask: {:08b}'.format(mask))
        #print('regval new: {:08b}'.format(regnew))
        return self.write_reg(reg, regnew)

    def set_initial(self):
        self.set_field(self.BUCKBST_CONF, "EN_BUCKBST", 1)                  # enable buckbst
        self.set_field(self.BUCKBST_CONF, "BUCKBST_ILIM", 0b111)            # buckbst I_lim -> 800mA

        self.set_field(self.CHARGER_VBUS_ILIM, "ILIM", 0b000)               # V_bus I_lim -> 50 mA
        self.set_field(self.CHARGER_TERMINATION_SETTING, "VTRM", 0b010110)  # V_trm -> 4.0 V
        self.set_field(self.CHARGER_CURRENT_SETTING, "ICHG", 0b10000)       # I_chg -> 170 mA

        self.set_field(self.BATTERY_THERMISTOR_CONTROL, "ITHR", 0b11)       # I_thr -> 6 uA - > 100 kOhm
        #self.set_field(self.BATTERY_THERMISTOR_CONTROL, "EN_THR", 0b11)     # enable thr current source with V_bus missing

        self.set_field(self.CHARGER_FUNCTION_SETTING, "EN_CHG", 1)          # enable charger
        #self.set_field(self.CHARGER_FUNCTION_SETTING, "EN_ADPICHG", 1)      # enable adaptive V_bus current limit
        #self.set_field(self.CHARGER_FUNCTION_SETTING, "OFF_ISOFET", 1)      # force turn off ISOFET

    def set_voltage(self, voltage, chan=None):
        if chan == "hi":
            maxval = 5.5
            minval = 1.8
            #v = max(minval, min(maxval, voltage))
            # safety for esp8285
            max_safe = 3.3
            v = max(minval, min(max_safe, voltage))
            if v < 2.95:
                code = int(round((v-minval)*10, 0))
            else:
                code = int(round(12+((v-2.95)*20), 0))
            self.set_field(self.BUCKBST_VOLT_SET, "VOUT_BUCKBST", code)
        elif chan == "lo":
            maxval = 3.75
            minval = 0.6
            v = max(minval, min(maxval, voltage))
            code = int(round((v-minval)*20, 0))
            self.set_field(self.BUCK_VOLT_SET, "VOUT_BUCK", code)
        else:
            raise Exception("No voltage channel selected ('hi' or 'lo').")

    def get_bat_temp(self, t_0=25.0, r_0=100000, beta=4303):
        import math
        lo = self.get_field(self.THR_VOLTAGE_LOW, "THR_V_LOW")
        hi = self.get_field(self.THR_VOLTAGE_HIGH, "THR_V_HIGH")
        hilo = hi << 8 | lo
        r = (hilo/6) * 1000
        steinhart = math.log(r / r_0) / beta
        steinhart += 1.0 / (t_0 + 273.15)
        steinhart = (1.0 / steinhart) - 273.15
        return round(steinhart, 2)

    def get_bat_voltage(self):
        lo = self.get_field(self.VBAT_READ_L, "VBAT_READ")
        hi = self.get_field(self.VBAT_READ_H, "VBAT_READ")
        hilo = hi << 8 | lo
        print(hi, lo)
        print(bin(hilo))
        print("V_bat [V]:", hilo/1000)
